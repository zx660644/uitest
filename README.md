## 使用方法

1. 将项目克隆至本地
2. 安装依赖包

```shell
pip install -r requirements.txt --default-timeout=200 -i https://pypi.tuna.tsinghua.edu.cn/simple
```

3. 参考`/src/cases/test_home.py`进行用例编写
4. 参考`Jenkinsfile`修改并编写流水线文件放到Jenkins上执行
5. 使用Jenkins定时或手动执行用例

![公众号](img/公众号.jpg)

[1.UI自动化测试框架搭建-yaml文件管理定位元素](https://mp.weixin.qq.com/s/JANJnoLWywv4ORuVpougfQ)

[2.UI自动化测试框架搭建-元素查找](https://mp.weixin.qq.com/s/Q9VWvYFi2eAKzvL1e9U-vw)

[3.UI自动化测试框架搭建-封装元素操作基类](https://mp.weixin.qq.com/s/AmUagYeLUt7qrfOiM2hsjA)

[4.UI自动化测试框架搭建-文件结构](https://mp.weixin.qq.com/s/kLe_Ojyg_EE6swPJ8p7KlA)

[5.UI自动化测试框架搭建-常用操作封装(一)](https://mp.weixin.qq.com/s/vMXEKoumw44GVCWJyJOV5A)

[6.UI自动化测试框架搭建-常用操作封装(二)](https://mp.weixin.qq.com/s/1jxBpONyFeFK6YlgMOqKvw)

[7.UI自动化测试框架搭建-失败后自动截图](https://mp.weixin.qq.com/s/bAPovftJfHQhKImOxwGwmA)

[8.UI自动化测试框架搭建-编写首个脚本](https://mp.weixin.qq.com/s/TX-nRNi1tf8ZRD7NglH34A)

[9.UI自动化测试框架搭建-使用Jenkinsfile管理测试流程](https://mp.weixin.qq.com/s/Hmdb48uhj_ZlX80zMRGvqg)

[10.UI自动化测试框架搭建-获取Jenkins参数](https://mp.weixin.qq.com/s/-k_V2f-Kf1unqqU7uGUs5A)

[11.UI自动化测试框架搭建-编写执行脚本入口](https://mp.weixin.qq.com/s/xKmToTf7LmnNihOdtmd-IQ)

[12.UI自动化测试框架搭建-编写一个APP自动化](https://mp.weixin.qq.com/s/E13iG3RqAYyZpp0UdBebSA)

[13.UI自动化测试框架搭建-处理Allure报告数据](https://mp.weixin.qq.com/s/LqDcOfyoKulAgako9gkKCw)

[14.UI自动化测试框架搭建-企业微信发送报告](https://mp.weixin.qq.com/s/xDFhUnKaui0sy4nx6nYH6Q)

[15.UI自动化测试框架搭建-借鉴AirTest框架Template定位元素](https://mp.weixin.qq.com/s/Sjndalrvhr8iNpP4cE_qeg)

[16.UI自动化测试框架搭建-添加Template方式定位元素](https://mp.weixin.qq.com/s/Ru00bzWZeaDm651qMoWnLw)

[17.UI自动化测试框架搭建-借助Sonic获取图片元素控件](https://mp.weixin.qq.com/s/3XDX7E0-sHxf42ggyIWjSA)

[18.UI自动化测试框架搭建-优化企业微信通知](https://mp.weixin.qq.com/s/VBaChebdthulHlAE0A6LTQ)

[19.UI自动化测试框架搭建-性能数据采集](https://mp.weixin.qq.com/s/0xqmcgwqmWdV8a_8lm_cow)

[20.UI自动化测试框架搭建-标记性能较差用例](https://mp.weixin.qq.com/s/kYujaPMDmmELaavjGJRdMA)