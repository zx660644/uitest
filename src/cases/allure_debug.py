# -*- coding: utf-8 -*-
# @Time    : 2022/3/10 2:50 PM
# @Author  : zhongxin
# @Email   : 490336534@qq.com
# @File    : allure_debug.py
import os
import sys

import pytest

BASE_PATH = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(BASE_PATH)
sys.path.append(os.path.join(BASE_PATH, 'src'))

from src.utils.constant import ALLURE_TOOL_PATH, REPORT_PATH, CONCURRENT, CASES, BUILD_URL

if CASES:
    test_cases = CASES.split('\n')
    main_list = [
        '-s',
        '-v',
        *test_cases,
        '--durations=0', '--clean-alluredir',
        '--alluredir', f'{REPORT_PATH}/allure_results'
    ]
else:
    main_list = [
        '-s',
        '-v',
        '--durations=0', '--clean-alluredir',
        '--alluredir', f'{REPORT_PATH}/allure_results'
    ]

if CONCURRENT != '否':
    main_list.append('-n')
    main_list.append(CONCURRENT)
    main_list.append('--dist')
    main_list.append('loadfile')
pytest.main(main_list)
if not BUILD_URL:
    os.system(f"{ALLURE_TOOL_PATH}/allure serve {REPORT_PATH}/allure_results")  # 本地执行
