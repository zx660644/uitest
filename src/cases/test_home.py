# -*- coding: utf-8 -*-
# @Time    : 2022/3/7 10:54 AM
# @Author  : zhongxin
# @Email   : 490336534@qq.com
# @File    : test_home.py
import time

import allure
import pytest

from src.utils.allureoperator import compose


# H5 Demo
@compose(feature="微医主站", story="首页", title='主入口下方文案校验')
@pytest.mark.parametrize("way", ["registered", "inquiry", "buy_medicine"])
def test_home_text(home, way):
    """
    按钮下方文案测试
        * 挂号
        * 问诊
        * 购药
    """
    ele = getattr(home, way)
    with allure.step(f"查看{ele.desc}按钮下方文案"):
        text = home.get_text(getattr(home, f"{way}_text"))
        if way == 'registered':
            assert text == '全国7800+家\n医院'
        elif way == 'inquiry':
            assert text == '28万医生在线\n服务'
        elif way == 'buy_medicine':
            assert text == '微医自营\n购药更安心'


# 安卓 Demo
@compose(feature="微医APP", story="首页", title='主入口下方文案校验')
@pytest.mark.parametrize("way", ["registered_text", "inquiry_text1", "inquiry_text2"])
def test_home_android_text(home_android, way):
    """
    按钮下方文案测试
        * 挂号
        * 问诊
        * 购药
    """
    ele = getattr(home_android, way)
    with allure.step(f"查看{ele.desc}"):
        assert home_android.has_element(ele), f'没有找到{ele.desc}'


# 安卓 Template方式定位Demo
@compose(feature="微医APP", story="首页", title='点击问诊图片')
def test_home_android_click_img(home_android):
    """
    点击问诊图片:使用Template方式定位演示
    """
    home_android.click(home_android.registered_img)
    time.sleep(10)
    print("test")
