# -*- coding: utf-8 -*-
# @Time    : 2022/3/2 3:07 PM
# @Author  : zhongxin
# @Email   : 490336534@qq.com
# @File    : HomePage.py
from src.utils.constant import BASE_DATA_PATH
from src.utils.elementoperator import ElementOperator


class HomePage(ElementOperator):
    def __init__(self, path=f"{BASE_DATA_PATH}/home.yaml", file_name=f'home_h5', driver=None):
        super(HomePage, self).__init__(path, file_name, driver)
